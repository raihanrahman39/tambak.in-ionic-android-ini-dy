import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { UserContentDashboardComponent } from './user-content-dashboard/user-content-dashboard.component';
import { UserContentKolamComponent } from './user-content-kolam/user-content-kolam.component';
import { UserContentKustomerComponent } from './user-content-kustomer/user-content-kustomer.component';
import { UserContentPegawaiComponent } from './user-content-pegawai/user-content-pegawai.component';
import { UserContentPerusahaan2Component } from './user-content-perusahaan2/user-content-perusahaan2.component';
import { UserContentProjectDetailComponent } from './user-content-project-detail/user-content-project-detail.component';
import { UserContentProjectComponent } from './user-content-project/user-content-project.component';
import { UserContentTugasComponent } from './user-content-tugas/user-content-tugas.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { UserpageComponent } from './userpage/userpage.component';

const routes: Routes = [
  {
    path:"", component:LoginpageComponent,
    children:[
      {
        path:"",
        component:UserLoginComponent
      },
      {
        path:"register",
        component:UserRegistrationComponent
      }
    ]
  },
  {
    path:"userpage", 
    component:UserpageComponent, 
    runGuardsAndResolvers: 'always',
    children:[
      {
        path:"",
        component:UserContentDashboardComponent,
      },
      {
        path:"kustomer",
        component:UserContentKustomerComponent
      },
      {
        path:"perusahaan",
        component:UserContentPerusahaan2Component
      },
      {
        path:"tugas",
        component:UserContentTugasComponent
      },
      {
        path:"kolam",
        component:UserContentKolamComponent
      },
      {
        path:"pegawai",
        component:UserContentPegawaiComponent
      },
      {
        path:"projects",
        component:UserContentProjectComponent
      },
      {
        path:"projects/:idProject",
        component:UserContentProjectDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
