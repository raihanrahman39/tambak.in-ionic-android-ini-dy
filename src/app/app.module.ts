import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AlertModule } from './_alert';
import { DataProviderService } from './data-provider.service';
import { FooterComponent } from './footer/footer.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { UserNavbarComponent } from './user-navbar/user-navbar.component';
import { UserSidebarComponent } from './user-sidebar/user-sidebar.component';
import { UserFooterComponent } from './user-footer/user-footer.component';
import { UserpageComponent } from './userpage/userpage.component';
import { UserContentPerusahaanComponent } from './user-content-perusahaan/user-content-perusahaan.component';
import { UserContentDashboardComponent } from './user-content-dashboard/user-content-dashboard.component';
import { LoginPerusahaanComponent } from './login-perusahaan/login-perusahaan.component';
import { UserContentKolamComponent } from './user-content-kolam/user-content-kolam.component';
import { UserContentPegawaiComponent } from './user-content-pegawai/user-content-pegawai.component';
import { UserContentTugasComponent } from './user-content-tugas/user-content-tugas.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { UserContentProjectComponent } from './user-content-project/user-content-project.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserContentPerusahaan2Component } from './user-content-perusahaan2/user-content-perusahaan2.component';
import { GrafikComponent } from './grafik/grafik.component';
import { UserContentProjectDetailComponent } from './user-content-project-detail/user-content-project-detail.component';
import { UserContentKustomerComponent } from './user-content-kustomer/user-content-kustomer.component';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    LoginpageComponent,
    UserNavbarComponent,
    UserSidebarComponent,
    UserFooterComponent,
    UserpageComponent,
    UserContentPerusahaanComponent,
    UserContentDashboardComponent,
    LoginPerusahaanComponent,
    UserContentKolamComponent,
    UserContentPegawaiComponent,
    UserContentTugasComponent,
    UserContentProjectComponent,
    UserRegistrationComponent,
    UserLoginComponent,
    UserContentPerusahaan2Component,
    GrafikComponent,
    UserContentProjectDetailComponent,
    UserContentKustomerComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AlertModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    DataProviderService, 
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
