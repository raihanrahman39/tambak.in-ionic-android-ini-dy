import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({providedIn: 'root'})
export class DataProviderService {
	  server: string ="http://express.tambak.in/"
	token: string

  	constructor(public http : HttpClient,  public router:Router) { 
    
  	}

  	postData(body, file){
		let type="application/json; charset=UTF-8";
		let headers= new HttpHeaders({
			'Content-Type': type,
			'Accept': 'application/json, text/plain, */*',     
			'Access-Control-Allow-Origin': 'http://tambak.in/',       
			'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',       
			'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
			"Authorization":"Bearer "+localStorage.getItem("token")
		});
		return this.http.post(this.server+file, body, {headers:headers})
		.pipe(
			catchError(error=>{
			  	if(error.status==403){
					this.router.navigateByUrl('userlogin')
			  	}else if(error.status==500){
					return {gagal:true, message:error.message}
				}
			  	return error
			}),
			map(data => {
			  	return data
			}),
		);
	}
  	getData(file){
		let type="application/json; charset=UTF-8";
		let headers= new HttpHeaders({
			'Content-Type': type,
			'Accept': 'application/json, text/plain, */*',
			'Access-Control-Allow-Origin': '*',       
			'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',       
			'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
			"Authorization":"Bearer "+localStorage.getItem("token")
		});
		return this.http.get(this.server+file, {headers:headers})
		.pipe(
			catchError(error=>{
			  	if(error.status==403){
					this.router.navigateByUrl('userlogin')
			  	}else if(error.status==500){
					return {gagal:true, message:error.message}
				}
			  	return error
			}),
			map(data => {
			  	return data
			}),
		  );
	}
	deleteData(file){
		var headers = new HttpHeaders;
		headers = headers.append("Content-Type","application/json")
		headers = headers.append("Accept", "application/json")
		headers = headers.append("Authorization", "Bearer "+ localStorage.getItem("token"))
		headers = headers.append("observe", 'response')
		return this.http.delete(this.server+file, {headers:headers})
		.pipe(catchError(error=>{
		  	if(error.status==403){
				this.router.navigateByUrl('userlogin')
		  	}else if(error.status==500){
				return {gagal:true, message:error.message}
			}
		  	return error
		}),
			map(data => {
		  		return data
		}),
		);
	}

	uploadData(body, file){
		let headers= new HttpHeaders({
		  'Access-Control-Allow-Origin': '*',
		  'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
		  'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
		  "Authorization":"Bearer "+localStorage.getItem("token")
		});
		return this.http.post(this.server+file, body, {headers:headers})
		.pipe(
		  catchError(error=>{
			  if(error.status==403){
			  this.router.navigateByUrl('userlogin')
			  }else if(error.status==500){
				return {gagal:true, message:error.message}
			  }
			  return error
		  }),
		  map(data => {
			  return data
		  }),
		);
	}
}
