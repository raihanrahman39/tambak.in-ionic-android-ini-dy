import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Chart } from 'chart.js'
@Component({
  selector: 'app-grafik',
  templateUrl: './grafik.component.html',
  styleUrls: ['./grafik.component.css']
})
export class GrafikComponent implements OnInit {
  @ViewChild('BarCanvas', {static:false}) BarCanvas:ElementRef;
  private barChart: Chart;
  warna=['rgb(255, 99, 132)','rgb(255, 159, 64)','rgb(255, 205, 86)','rgb(75, 192, 192)','rgb(54, 162, 235)','rgb(153, 102, 255)','rgb(201, 203, 207)'];
  config={
    type: 'line',
    data: {
      labels: [
      ],
      datasets: [{
        label: 'Processing . . .',
        data: [],
        borderWidth: 1,
        backgroundColor:'rgb(255,99,132)'
      }]
    },
        options: {
          responsive: true,
          title: {
            display: true,
            text: "Grafik MBW"
          },
          legend: {
            display: true
          },
          scales:{
              yAxes:[{
                ticks:{
                  beginAtZero:true
                },
                scaleLabel:{
                  display:true,
                  labelString:'MBW(kg)'
                }
              }],
              xAxes:[{
                scaleLabel:{
                  display:true,
                  labelString:'Hari'
                }
              }]
          }
        }
    };
  constructor(
    private apiPvdr:DataProviderService
  ) { }

  ngOnInit(): void {
    }
  ngAfterViewInit(){
    this.barChart = new Chart(this.BarCanvas.nativeElement,this.config)
    this.loadData()

  }
  loadData(){
    let body={
    }
    this.apiPvdr.postData(body, 'prediksi/udang/vaname/mbw').subscribe((data)=>{
      this.config.data.datasets[0].label = "Prediksi"
      this.config.data.datasets[0].data = data["mbw"]["prediksi"]
      this.config.data.labels = data["mbw"]["label"]
      if(this.barChart){
        this.barChart.destroy();
      }
      this.barChart = new Chart(this.BarCanvas.nativeElement, this.config);
    })

  }
}
