import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HalamanUserComponent } from './halaman-user.component';

describe('HalamanUserComponent', () => {
  let component: HalamanUserComponent;
  let fixture: ComponentFixture<HalamanUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HalamanUserComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HalamanUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
