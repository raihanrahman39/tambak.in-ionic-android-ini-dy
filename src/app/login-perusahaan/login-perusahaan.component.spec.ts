import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPerusahaanComponent } from './login-perusahaan.component';

describe('LoginPerusahaanComponent', () => {
  let component: LoginPerusahaanComponent;
  let fixture: ComponentFixture<LoginPerusahaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPerusahaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPerusahaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
