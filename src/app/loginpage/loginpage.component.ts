import { Component, OnInit } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  content = 'login';

  constructor(
    private dp:DataProviderService, 
    private router:Router, 
    protected alertService: AlertService) { }
  ngOnInit(): void {
  }
  
  clicked(konten){
    document.getElementById(this.content).classList.remove("aactive")
    document.getElementById(konten).classList.add("aactive")
    this.content = konten
    if(konten=='login'){
      this.router.navigate(['/'])
    }else{
      this.router.navigateByUrl('/register')
    }
  }

}
