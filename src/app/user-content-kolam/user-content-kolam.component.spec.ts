import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentKolamComponent } from './user-content-kolam.component';

describe('UserContentKolamComponent', () => {
  let component: UserContentKolamComponent;
  let fixture: ComponentFixture<UserContentKolamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentKolamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentKolamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
