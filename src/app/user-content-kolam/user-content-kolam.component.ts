import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-kolam',
  templateUrl: './user-content-kolam.component.html',
  styleUrls: ['./user-content-kolam.component.css']
})
export class UserContentKolamComponent implements OnInit {
  kolam: any=[];
  status: boolean;
  hakakses = localStorage.getItem("status")

  namaKolam: string;
  longitude: string;
  latitude: string;
  luas: string;

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) {   }

  ngOnInit(): void {
    this.getDataKolam()
  }
  ionViewWillEnter(){
    this.getDataKolam()
  }
  cleardata(){
    this.namaKolam = ''
    this.longitude = ''
    this.latitude = ''
    this.luas = ''
  }

  addKolam(){
    this.alertService.clear();
    let body = {
      namaKolam: this.namaKolam,
      longitude: this.longitude,
      latitude: this.latitude,
      luas: this.luas
    }
    this.dp.postData(body, "kolam").subscribe((data)=>{
      if(data["affectedRows"] == 1){
        this.getDataKolam()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      }  
    })
    this.cleardata()
  }
  
  getDataKolam() {
    this.dp.getData("kustomer/kolam").subscribe((data)=>{
      this.kolam = data;
    })
  }
  deleteKolam(id: String) {
    this.dp.deleteData("kolam/"+id).subscribe((data)=>{
      this.getDataKolam()
    })
  }

  ubahKolam(id = String) {

  }

  clickeditkolam(nama:string,long:string,lat:string,lu:string){
    this.namaKolam = nama
    this.longitude = long
    this.latitude = lat
    this.luas = lu
  }
}
