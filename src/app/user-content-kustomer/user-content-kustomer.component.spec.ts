import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentKustomerComponent } from './user-content-kustomer.component';

describe('UserContentKustomerComponent', () => {
  let component: UserContentKustomerComponent;
  let fixture: ComponentFixture<UserContentKustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentKustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentKustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
