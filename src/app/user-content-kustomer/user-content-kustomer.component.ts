import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';


@Component({
  selector: 'app-user-content-kustomer',
  templateUrl: './user-content-kustomer.component.html',
  styleUrls: ['./user-content-kustomer.component.css']
})
export class UserContentKustomerComponent implements OnInit {
  kustomer: any=[];
  kontak: any=[];
  status: boolean;

  namaDepan: string;
  namaBelakang: string;
  username: string;
  passwordLama: string;
  passwordBaru: string;
  passwordConfirm: string;
  namaKontak:string;
  isiKontak:string;

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { }

  ngOnInit(): void {
    this.getDataKustomer()
    this.getDataKontak()
  }
  
  ionViewWillEnter(){
    this.getDataKustomer()
    this.getDataKontak()
  }
  
  getDataKontak() {
    this.dp.getData("kontak").subscribe((data)=>{
      this.kontak = data;
    })
  }
  getDataKustomer() {
    this.dp.getData("kustomer/detail").subscribe((data)=>{
      this.kustomer = data;
    })
  }
  ubahKustomer(){
    this.alertService.clear();
    let body = {
      namaDepan: this.namaDepan,
      namaBelakang: this.namaBelakang,
      username: this.username,
      passwordLama: this.passwordLama,
      passwordBaru: this.passwordBaru
    }
    this.dp.postData(body, "linkgantikustomer").subscribe((data)=>{
      if(data["affectedRows"] == 1){
        this.getDataKustomer()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      }
    })
  }

  gantiPassword(){
    if(this.passwordBaru == this.passwordConfirm){
      this.alertService.clear();
      let body = {
        namaDepan: this.namaDepan,
        namaBelakang: this.namaBelakang,
        username: this.username,
        passwordLama: this.passwordLama,
        passwordBaru: this.passwordBaru
      }
      this.dp.postData(body, "linkgantikustomer").subscribe((data)=>{
        if(data["affectedRows"] == 1){
          this.getDataKustomer()
          window.location.reload()
        } else{
          this.alertService.error("Ada kesalahan silahkan coba lagi");
        }
      })
    }else{
      this.alertService.error("Password tidak sama");
    }
  }

  addKontak(){
    this.alertService.clear();
    let body = {
      namaKontak: this.namaKontak,
      isiKontak: this.isiKontak
    }
    this.dp.postData(body, "kontak").subscribe((data)=>{
      if(data["affectedRows"] == 1){
        this.kontak = ''
        this.getDataKontak()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      }
    })
  }

  ubahKontak(id:string){
    this.alertService.clear();
    let body = {
      namaKontak: this.namaKontak,
      isiKontak: this.isiKontak
    }
    this.dp.postData(body, "kontak/"+id).subscribe((data)=>{
      if(data["affectedRows"] == 1){
        this.getDataKontak()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      }
    })
  }

  deleteKontak(id:string){
    this.dp.deleteData("kontak/"+id).subscribe((data)=>{
      this.kontak = ''
      this.getDataKontak()
      window.location.reload()
    })
  }

  clickeditkustomer() {
    this.namaDepan = this.kustomer.namaDepan
    this.namaBelakang = this.kustomer.namaBelakang
    this.username = this.kustomer.username
  }

  clickeditkontak(nama: string, isi:string) {
    this.namaKontak = nama
    this.isiKontak = isi
  }
}
