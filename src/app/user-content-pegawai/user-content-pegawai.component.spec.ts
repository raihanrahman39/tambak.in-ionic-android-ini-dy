import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentPegawaiComponent } from './user-content-pegawai.component';

describe('UserContentPegawaiComponent', () => {
  let component: UserContentPegawaiComponent;
  let fixture: ComponentFixture<UserContentPegawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentPegawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentPegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
