import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-pegawai',
  templateUrl: './user-content-pegawai.component.html',
  styleUrls: ['./user-content-pegawai.component.css']
})
export class UserContentPegawaiComponent implements OnInit {
  pegawai: any=[];
  status: boolean;
  hakakses = localStorage.getItem("status")

  constructor( private dp:DataProviderService, private router:Router, protected alertService: AlertService) { }
  
  getDataPegawai() {
    this.dp.getData("perusahaan/pegawai").subscribe((data)=>{
      this.pegawai = data;
    })
  }
  ngOnInit(): void {
    this.getDataPegawai()
  }

  ionViewWillEnter(){
    this.getDataPegawai()
  }
  deletePegawai(id: number){

  }

  ubahPegawai(id: number, hak: string){

  }
}
