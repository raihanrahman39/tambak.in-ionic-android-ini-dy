import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentPerusahaanComponent } from './user-content-perusahaan.component';

describe('UserContentPerusahaanComponent', () => {
  let component: UserContentPerusahaanComponent;
  let fixture: ComponentFixture<UserContentPerusahaanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentPerusahaanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentPerusahaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
