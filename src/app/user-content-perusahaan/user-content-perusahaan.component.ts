import { Component, OnInit, Input } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';

import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-perusahaan',
  templateUrl: './user-content-perusahaan.component.html',
  styleUrls: ['./user-content-perusahaan.component.css']
})
export class UserContentPerusahaanComponent implements OnInit {
  @Input() perusahaan: any=[];
  status: boolean;
  message: string;
  gambarserver = 'https://tambakin.re-beat.xyz/public/images';

  namaPerusahaan: string;
  alamatPerusahaan: string;
  gambar: File;
  imgurl: any;

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { 
  }

  ngOnInit(): void {
  }

  addPerusahaan(){
    this.alertService.clear();
    let body = new FormData();
    body.append("imageFile", this.gambar)
    body.append('namaPerusahaan', this.namaPerusahaan)
    body.append('alamatPerusahaan', this.alamatPerusahaan)
    this.dp.uploadData(body, "perusahaan/").subscribe((data)=>{
      if(data["success"]){
        window.location.reload();
      } else{
        this.status = true;
        this.message = data['message'];
        this.alertService.error(this.message);
      } 
    })
  }

  fileSelected(event){
    if(event.target.files.length > 0){
      const file = event.target.files[0]
      this.gambar = file
      const reader = new FileReader();
      reader.onload = e => this.imgurl = reader.result;

      reader.readAsDataURL(file);
    }
  }
}
