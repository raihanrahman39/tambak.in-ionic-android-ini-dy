import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentPerusahaan2Component } from './user-content-perusahaan2.component';

describe('UserContentPerusahaan2Component', () => {
  let component: UserContentPerusahaan2Component;
  let fixture: ComponentFixture<UserContentPerusahaan2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentPerusahaan2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentPerusahaan2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
