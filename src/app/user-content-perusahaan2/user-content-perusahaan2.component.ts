import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-perusahaan2',
  templateUrl: './user-content-perusahaan2.component.html',
  styleUrls: ['./user-content-perusahaan2.component.css']
})
export class UserContentPerusahaan2Component implements OnInit {
  perusahaan: any=[];
  status: boolean;
  message: string;
  gambarserver = 'http://express.tambak.in/images/perusahaan/';

  hakakses = localStorage.getItem("status")
  namaPerusahaan: string;
  alamatPerusahaan: string;
  gambar: File;
  imgurl: any;
  idPerusahaan: number;

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { }

  ngOnInit(): void {
    this.getDataPerusahaan()
  }
  ionViewWillEnter(){
    this.getDataPerusahaan()
  }
  addPerusahaan(){
    this.alertService.clear();
    let body = new FormData();
    body.append('namaPerusahaan', this.namaPerusahaan)
    body.append('alamatPerusahaan', this.alamatPerusahaan)
    if(this.gambar){
      body.append("imageFile", this.gambar)
    }
    this.dp.uploadData(body, "perusahaan/").subscribe((data)=>{
      if(data['success']){
        localStorage.setItem("status",data[ 'status'])
        localStorage.setItem("token", data['token'])
        this.getDataPerusahaan()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      } 
    })
  }
  ubahPerusahaan(){
    this.alertService.clear();
    let body = new FormData();
    body.append('namaPerusahaan', this.namaPerusahaan)
    body.append('alamatPerusahaan', this.alamatPerusahaan)
    if(this.gambar){
      body.append("imageFile", this.gambar)
    }
    this.dp.uploadData(body, "perusahaan/edit").subscribe((data)=>{
      if(data["affectedRows"] == 1){  
        this.getDataPerusahaan()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      } 
    })
  }
  
  getDataPerusahaan() {
    this.dp.getData("kustomer/perusahaan").subscribe((data)=>{
      this.perusahaan = data[0];
    })
  }
  joinPerusahaan(){
    this.alertService.clear();
    let body = {
      idPerusahaan: this.idPerusahaan
    }
    this.dp.postData(body, "kustomer/perusahaan/").subscribe((data)=>{
      if(data['success']){
        console.log(data)
        localStorage.setItem("status",data[ 'status'])
        this.getDataPerusahaan()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      } 
    })
  }

  deletePerusahaan(){
    
  }

  keluarPerusahaan(){
    this.dp.deleteData("kustomer/perusahaan").subscribe((data)=>{
      localStorage.setItem("token", data["token"])
      localStorage.setItem("status", data["status"])
      this.getDataPerusahaan()
      window.location.reload()
    })
  }

  fileSelected(event){
    if(event.target.files.length > 0){
      const file = event.target.files[0]
      this.gambar = file
      const reader = new FileReader();
      reader.onload = e => this.imgurl = reader.result;

      reader.readAsDataURL(file);
    }
  }

  clickeditperusahaan() {
    this.namaPerusahaan = this.perusahaan.namaPerusahaan
    this.alamatPerusahaan = this.perusahaan.alamatPerusahaan
  }
}
