import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentProjectDetailComponent } from './user-content-project-detail.component';

describe('UserContentProjectDetailComponent', () => {
  let component: UserContentProjectDetailComponent;
  let fixture: ComponentFixture<UserContentProjectDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentProjectDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentProjectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
