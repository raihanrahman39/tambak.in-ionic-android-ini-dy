import { Component, OnInit, Input } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-project-detail',
  templateUrl: './user-content-project-detail.component.html',
  styleUrls: ['./user-content-project-detail.component.css']
})
export class UserContentProjectDetailComponent implements OnInit {
  @Input() proyekd: any=[];
  status: boolean;

  namaProyek: string
  komoditas: string
  
  namaKolam: string
  luas: number
  lamaSiklus: number

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { }

  ngOnInit(): void {
  }

  addKolam(){

  }

  ubahProyek(){

  }

  deleteKolam(id:number){

  }

  clickeditproyek(){

  }
}
