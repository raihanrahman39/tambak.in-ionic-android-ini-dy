import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentProjectComponent } from './user-content-project.component';

describe('UserContentProjectComponent', () => {
  let component: UserContentProjectComponent;
  let fixture: ComponentFixture<UserContentProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
