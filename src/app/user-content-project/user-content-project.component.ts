import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-content-project',
  templateUrl: './user-content-project.component.html',
  styleUrls: ['./user-content-project.component.css']
})
export class UserContentProjectComponent implements OnInit {
  proyek: any=[];
  proyekd: any=[]
  status: boolean;
  hakakses = localStorage.getItem("status")

  namaProyek: string;
  komoditas: string;

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { }
  ionViewWillEnter(){
    this.getDataProyek()
  }
  getDataProyek() {
    this.dp.getData("proyek/groupby").subscribe((data)=>{
      this.proyek = data;
    })
  }
  ngOnInit(): void {
    this.getDataProyek()
  }

  addProyek(){
    this.alertService.clear();
    let body = {
      namaProyek: this.namaProyek,
      komoditas: this.komoditas
    }
    this.dp.postData(body, "proyek").subscribe((data)=>{
      if(data["affectedRows"] == 1){
        this.getDataProyek()
        window.location.reload()
      } else{
        this.alertService.error("Ada kesalahan silahkan coba lagi");
      }  
    })
  }

  deleteProyek(id:number){

  }
  
  getDataProyekD(id:number) {
    this.dp.getData("proyek/"+id+"/kolam").subscribe((data)=>{
      this.proyekd = data;
      console.log(data)
    })
  }
}
