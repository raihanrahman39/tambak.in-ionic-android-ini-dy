import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentTugasComponent } from './user-content-tugas.component';

describe('UserContentTugasComponent', () => {
  let component: UserContentTugasComponent;
  let fixture: ComponentFixture<UserContentTugasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentTugasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentTugasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
