import { Component, OnInit } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  username: string;
  password: string;
  status: boolean;
  message: string;

  constructor(
    private dp:DataProviderService, 
    private router:Router, 
    protected alertService: AlertService,
    private loadingCtrl: LoadingController) { }

  ngOnInit(): void {
  }

  async loginin() {
    let loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    loading.present()
    let body = {
      username : this.username,
      password : this.password
    }
    this.dp.postData(body, "kustomer/login").subscribe((data)=>{
      loading.dismiss()
      if(data["success"]){
        localStorage.setItem("token", data['token'])
        localStorage.setItem("username", this.username)
        localStorage.setItem("status",data["status"])
        this.router.navigateByUrl('userpage')
      } else{
        this.status = true;
        this.message = data['message'];
        this.alertService.error(this.message);
      } 
    })
  }
}
