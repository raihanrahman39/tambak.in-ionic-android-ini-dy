import { Component, OnInit } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {
  namaDepan: string;
  namaBelakang: string;
  username: string;
  password: string;
  status: boolean;
  message: string;
  confirmPassword: string;
  token = localStorage.getItem("token");

  constructor(
    private dp:DataProviderService, 
    private router:Router, 
    protected alertService: AlertService) { }

  ngOnInit(): void {
  }

  daftarin() {
    this.alertService.clear();
    if(this.password == this.confirmPassword){
      let body = {
        username : this.username,
        password : this.password,
        namaDepan: this.namaDepan,
        namaBelakang: this.namaBelakang,
      }
      this.dp.postData(body, "kustomer/signup").subscribe((data)=>{
        console.log(data)
        if(data["success"]){
          window.location.reload();
        } else{
          this.status = true;
          this.message = data['message'];
          this.alertService.error(this.message);
        } 
      })
    } else {
      this.alertService.error("Password tidak sama");
    }
  }
}
