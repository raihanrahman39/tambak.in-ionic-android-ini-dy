import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataProviderService } from '../data-provider.service';
import { Router, NavigationEnd } from '@angular/router';
import { AlertService } from '../_alert';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit, OnDestroy {
  navigationSubscription;
  content = 'dashboard';
  username = localStorage.getItem("username");
  kustomer: any=[];

  constructor(private dp:DataProviderService, private router:Router, protected alertService: AlertService) { 
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.getDataKustomer()
      }
    });
  }
  ngOnDestroy(): void {
    if (this.navigationSubscription) {  
      this.navigationSubscription.unsubscribe();
    }
  }
  
  getDataKustomer() {
    this.dp.getData("kustomer/detail").subscribe((data)=>{
      this.kustomer = data;
    })
  }

  ngOnInit(): void {
  }

  clicked(konten){
    document.getElementById(this.content).classList.remove("active")
    this.content = konten;
    document.getElementById(this.content).classList.add("active")
  }

  logout(){
    localStorage.removeItem("token")
    localStorage.removeItem("username")
    this.router.navigateByUrl('userlogin')
  }



}
